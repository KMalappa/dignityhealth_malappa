import { TestBed, async } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { AdminComponent } from './admin/admin.component';
import { NavigationComponent } from './admin/navigation/navigation.component';
import { Globals } from './globals';

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule, RouterTestingModule],
      providers : [
        Globals
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        AdminComponent,
        NavigationComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'loading true!'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    console.log(app);
    expect(app.globals.loading).toEqual(true);
  }));

  it('should render user link check on li>a tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    console.log(compiled);
    console.log(compiled.querySelector('.nav-dropdown li>a').textContent);
    expect(compiled.querySelector('.nav-dropdown li>a').textContent).toContain('Users');
  }));
});
