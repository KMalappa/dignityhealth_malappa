import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';

import { WhiteListService } from './services/white-list.service';

import { NavigationModule } from './navigation/navigation.module';
import { WhiteListDomainsModule } from './white-list-domains/white-list-domains.module';
import { WhiteListUsersModule } from './white-list-users/white-list-users.module';
import { AdminRoutingModule } from './admin-routing.module';

@NgModule({
  imports: [
    CommonModule,
    WhiteListDomainsModule,
    WhiteListUsersModule,
    NavigationModule,
    AdminRoutingModule
  ],

  declarations: [
    AdminComponent
  ],

  exports: [
    AdminComponent
  ],

  providers:[
    WhiteListService
  ]
})

export class AdminModule { }
