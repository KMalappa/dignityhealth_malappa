import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Http, Response, Headers, HttpModule, ConnectionBackend, RequestOptions, BaseRequestOptions } from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import { WhiteListDomainsComponent } from './white-list-domains.component';
import { APP_CONFIG } from '../../app.config';
import { APP_DI_CONFIG } from '../../app.config';
import { ComponentLoaderFactory } from 'ngx-bootstrap/component-loader';
import { PositioningService } from 'ngx-bootstrap/positioning';
import { BsModalService } from 'ngx-bootstrap/modal';
import { WhiteListService } from '../services/white-list.service';
import { Globals } from '../../globals';




describe('WhiteListDomainsComponent', () => {
  let component: WhiteListDomainsComponent;
  let fixture: ComponentFixture<WhiteListDomainsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [ WhiteListDomainsComponent ],
      providers : [
        PositioningService,
        ComponentLoaderFactory,
        Globals,
        {provide: ConnectionBackend, useClass: MockBackend},
        {provide: RequestOptions, useClass: BaseRequestOptions},
        WhiteListService,
        BsModalService,
        Http,
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteListDomainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
