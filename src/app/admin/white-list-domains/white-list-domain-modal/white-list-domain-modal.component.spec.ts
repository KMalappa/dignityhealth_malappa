import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhiteListDomainModalComponent } from './white-list-domain-modal.component';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

describe('WhiteListDomainModalComponent', () => {
  let component: WhiteListDomainModalComponent;
  let fixture: ComponentFixture<WhiteListDomainModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhiteListDomainModalComponent ],
      providers: [BsModalRef]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteListDomainModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
