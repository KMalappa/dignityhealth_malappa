import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { WhiteListDomains } from '../../modals/white-list-domains.modal';
import { WhiteListUser } from '../../modals/white-list-users.modal';

import { APP_CONFIG }  from '../../app.config';
import { AppConfig }  from '../.././app.config.interface';

@Injectable()
export class WhiteListService {

  getWhiteListUsersUrl: string;

  postWhiteListUserUrl: string;

  headers = new Headers({'Content-Type': 'application/json'});

  constructor(
    @Inject(APP_CONFIG) config: AppConfig,

    public http: Http
  ) { 

    this.getWhiteListUsersUrl = config.baseUrl + config.getWhiteListUsers;

    this.postWhiteListUserUrl = config.baseUrl + config.postWhiteListUser;

  }

  getWhiteListUsers(): Observable<any>{

    return this.http.get(this.getWhiteListUsersUrl)
        .map((response:Response) => response.json());

    /*return this.http.get("../../assets/mocks/white-list-users.mock.json")
      .map((response:Response) => response.json()); */
        
  };

  addWhiteListUser(usersModal): Observable<any>{

    return this.http.post(

      this.postWhiteListUserUrl,

      usersModal, 

      this.headers
    )
    .map((response:Response) => response.json());

  };

  getWhiteListDomains(): Observable<any>{

    return this.http.get("../../assets/mocks/white-list-domains.mock.json")
        .map((response:Response) => response.json());

  };
}
