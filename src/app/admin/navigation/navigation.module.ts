import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule  } from '@angular/router';

import { NavigationComponent } from './navigation.component';
import {DropdownModule} from "ng2-dropdown";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DropdownModule
  ],

  declarations: [
    NavigationComponent
  ],

  exports: [
    NavigationComponent
  ]
})

export class NavigationModule { }
