import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from '../../common/header/header.component'

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  navigationShowHide: boolean;
  navToggle: boolean;
  
  constructor() { 
    this.navigationShowHide = true;
    this.navToggle = false;
  }

  ngOnInit() {
  }

  changeNavigationStatus():void {
    this.navigationShowHide = !this.navigationShowHide;
  }

  navStatus():void {
    this.navToggle = true;
  }

  navStatusClose():void {
    this.navToggle = false;
  }


}
