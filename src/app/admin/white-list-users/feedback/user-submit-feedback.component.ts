import { Component, OnInit } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-user-submit-feedback',
  templateUrl: './user-submit-feedback.component.html',
  styleUrls: ['./user-submit-feedback.component.css']
})
export class UserSubmitFeedbackComponent implements OnInit {

  constructor(
    public bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
  }

}
