import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { WhiteListUsersComponent } from './white-list-users.component';

import { 
  UserSubmitFeedbackComponent
} from './feedback/user-submit-feedback.component';

import { ModalModule } from 'ngx-bootstrap';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    ReactiveFormsModule
  ],

  declarations: [
    WhiteListUsersComponent,
    UserSubmitFeedbackComponent
  ],

  entryComponents:[
    UserSubmitFeedbackComponent
  ]
})
export class WhiteListUsersModule { }
