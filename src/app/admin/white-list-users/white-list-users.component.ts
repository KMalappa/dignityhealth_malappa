import { Component, OnInit, ViewChild } from '@angular/core';

import { WhiteListUser } from '../../modals/white-list-users.modal';

import { NewWhiteListUser } from '../../modals/new-white-list-user.modal';

import { WhiteListService } from '../services/white-list.service';

import { BsModalService } from 'ngx-bootstrap/modal';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import {
  UserSubmitFeedbackComponent
} from './feedback/user-submit-feedback.component';

import { Globals } from '../../globals';

import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-white-list-users',
  templateUrl: './white-list-users.component.html',
  styleUrls: ['./white-list-users.component.css']
})
export class WhiteListUsersComponent implements OnInit {

  selectedItem: any;

  whiteListUsers: Array<WhiteListUser>;

  filteredUsers: Array<WhiteListUser>;

  totalUsers: number = 0;

  userModal: NewWhiteListUser;

  usersForm: FormGroup;

  users: FormArray;

  formErrors: Array<any>;

  validationMessages: any;

  Applications: String[] = ['BNI Curbside Submitter'];

  Prefixes: String[] = ['Dr.', 'Jr.', 'Mr.', 'Mrs.'];

  bsModalRef: BsModalRef;

  sortOrder: number;

  columnProperty: string;

  sortWithFirstName: boolean;

  sortWithLastName: boolean;

  filterWithStatus: boolean;

  @ViewChild('userModalPopup') userModalPopup;

  constructor(

    public whiteListService: WhiteListService,

    public bsModalService: BsModalService,

    public globals: Globals,

    public formBuilder: FormBuilder

  ) {

    this.sortWithFirstName = false;

    this.sortWithLastName = false;

    this.filterWithStatus = false;

  }

  ngOnInit() {

    this.globals.loading = true;

    this.getWhiteListUsers();

    this.buildUserForm();

  };

  buildUserForm(){

    this.usersForm = this.formBuilder.group({

      users: this.formBuilder.array([this.createUser()])
      
    });

    this.usersForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    //this.onValueChanged(); // (re)set validation messages now

    this.formErrors = [
      {
        'Application': '',
        'EmailAddress': '',
        'Prefix': '',
        'FirstName': '',
        'LastName': ''
      } 
    ];

    this.validationMessages = {

      'Application': {
        'required': 'Application is Required.'
      },

      'EmailAddress': {
        'required': 'Email Address is Required.',
        'pattern': 'Wrong Email Pattern.'
      },

      'Prefix': {
        'required': 'Prefix is Required.'
      },

      'FirstName': {
        'required': 'First Name is Required.',
        'pattern' : 'Only Characters And Numbers Are Allowed',
        'maxlength' : 'Maximum length for First Name is 25'
      },

      'LastName': {
        'required': 'Last Name is Required.',
        'pattern' : 'Only Characters And Numbers Are Allowed',
        'maxlength' : 'Maximum length for Last Name is 25'
      }

    };

  }  

  addUser(): void {
    this.users = this.usersForm.get('users') as FormArray;

    if(this.users.length < 10){ //maximum records to add is 10
      this.users.push(this.createUser());
      this.formErrors.push(this.createError());
    }

    return;
  }

  removeUser(index : number): void {
    this.users = this.usersForm.get('users') as FormArray;
    this.users.removeAt(index);
    
    this.formErrors.splice(index, 1);
  }

  createUser(): FormGroup {
    return this.formBuilder.group({
      Application: ['BNI Curbside Submitter', Validators.required],
      EmailAddress: ['', Validators.required],
      Prefix: ['Dr.', Validators.required],
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required]
    });
  };

  createError(): any{
    return {
      'Application': '',
      'EmailAddress': '',
      'Prefix': '',
      'FirstName': '',
      'LastName': ''
    };
  };

  getUsersFormGroup(field : string): any{
    return this.usersForm.get(''+field);
  }

  onValueChanged(data? : any) : void {
    if (!this.usersForm) { return; }

    let formLength = this.formErrors.length;

    let self =  this;

    for (let i = 0; i < formLength; i++) {

      if( self.getUsersFormGroup('users').controls ) {

        self.getUsersFormGroup('users').controls.forEach((element, index) => {

            for(let subfield in element.controls) {

              self.formErrors[i][subfield] = '';

              let control = element.controls[subfield];

              if (control && control.dirty && !control.valid) {

                let messages = self.validationMessages[subfield];

                for (let key in control.errors) {

                  self.formErrors[index][subfield] = messages[key] + '';
                  //console.log(self.formErrors);

                }

              }
            }
        });
      } else {
        return;
      }
    }

  };

  getWhiteListUsers() {

    this.globals.loading = true;

    this.whiteListService.getWhiteListUsers()
      .subscribe(result => {

        this.whiteListUsers = result.Data.Users;

        //this.whiteListUsers = result;

        this.filteredUsers = this.whiteListUsers;

        this.totalUsers = this.whiteListUsers.length;

        this.globals.loading = false;

        this.sortUsers(1, "LastName");
      })
  };

  sortUsers(sortOrder, columnProperty) {

    this.sortOrder = sortOrder;

    this.columnProperty = columnProperty;

    this.filteredUsers.sort((user1, user2) => {

      if (user1[columnProperty].toLocaleLowerCase() < user2[columnProperty].toLocaleLowerCase()) {

        return -1 * sortOrder;

      } else if (user1[columnProperty].toLocaleLowerCase() > user2[columnProperty].toLocaleLowerCase()) {

        return 1 * sortOrder;

      } else {

        return 0;

      }
    });
  }

  filterByStatus(selectedStatus: string): void {

    if (selectedStatus.toLocaleLowerCase() == "all") {

      this.filteredUsers = this.whiteListUsers;

      this.totalUsers = this.filteredUsers.length;

      this.sortUsers(this.sortOrder, this.columnProperty);

      return;

    }

    this.filteredUsers = this.whiteListUsers.filter((user) => {

      return user.Status.toLocaleLowerCase() == selectedStatus.toLocaleLowerCase();

    });

    this.sortUsers(this.sortOrder, this.columnProperty);

    this.totalUsers = this.filteredUsers.length;
  }


  showFirstNameSortOptions(): void {

    this.sortWithFirstName = !this.sortWithFirstName;

    this.sortWithLastName = false;

    this.filterWithStatus = false;

  }

  showLastNameSortOptions(): void {

    this.sortWithLastName = !this.sortWithLastName;

    this.sortWithFirstName = false;

    this.filterWithStatus = false;

  }

  showFilterOptions(): void {

    this.filterWithStatus = !this.filterWithStatus;

    this.sortWithFirstName = false;

    this.sortWithLastName = false;

  }

  addNewWhiteListUsers(usersForm, flag) {

    let request = [];

    this.userModalPopup.hide();

    this.globals.loading = true;

    request = this.createRequest(usersForm, flag);       

    this.whiteListService.addWhiteListUser(request)
      .subscribe(response => {

        this.globals.loading = false;

        this.bsModalRef = this.bsModalService.show(UserSubmitFeedbackComponent);

        this.bsModalRef.content.title = 'Input Record(s)';

        this.bsModalRef.content.feedback = response.Message;

        setTimeout(() => {

          this.bsModalRef.hide();

          this.getWhiteListUsers();

        },

          2000

        );

      })

  }

  createRequest(usersForm, flag){    

    let request = [];

    if(usersForm.users.length == 1){
      request = [
        {
          "Prefix" : usersForm.users[0].Prefix,
          "FirstName": usersForm.users[0].FirstName,
          "LastName": usersForm.users[0].LastName,
          "Email": usersForm.users[0].EmailAddress,
          "AppId": 1,
          "Invite": flag
        }
      ];

    } else {
      usersForm.users.forEach(element => {

        request.push(
          {
            "Prefix" : element.Prefix,
            "FirstName": element.FirstName,
            "LastName": element.LastName,
            "Email": element.EmailAddress,
            "AppId": 1,
            "Invite": flag
          }
        );

      });
    } 
    
    return request;
  }

  selectedClass(item) : void {

    this.selectedItem = item;

  };

  isActive(item) : any {

    return this.selectedItem == item;

  };

}


