import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { HeaderModule } from './common/header/header.module';
import { AdminModule } from './admin/admin.module';
import { AppComponent } from './app.component';

import { APP_CONFIG, APP_DI_CONFIG }  from './app.config';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';

import { Globals } from './globals';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HeaderModule,
    AdminModule,
    AppRoutingModule,
  ],

  providers: [
    Globals,
    {
      provide: APP_CONFIG, 
      useValue: APP_DI_CONFIG 
    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
