import { OpaqueToken  } from '@angular/core';

import { AppConfig  } from './app.config.interface';

export const APP_DI_CONFIG: AppConfig = {

  baseUrl: 'http://dev-curbsideapi-devslot-curbsideapi.azurewebsites.net/api',

  getWhiteListUsers: "/v1.0/80C641E8-E12F-406A-BB31-9557CE7D9F66/whitelist",

  postWhiteListUser : "/v1.0/80C641E8-E12F-406A-BB31-9557CE7D9F66/whitelistUser"

};

export let APP_CONFIG = new OpaqueToken('app.config');