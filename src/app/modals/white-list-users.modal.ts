export class WhiteListUser {

  constructor(

    public UserId: number,

    public Prefix: string,

    public FirstName: string,

    public LastName: string,

    public EmailAddress: string,

    public ClientApplicationId: number,

    public Status: string
    
  ) { }
}