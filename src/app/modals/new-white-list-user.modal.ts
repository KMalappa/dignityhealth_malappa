export class NewWhiteListUser {

  constructor(

    public Application: string,

    public EmailAddress: string,

    public Prefix: string,

    public FirstName: string,

    public LastName: string
    
  ) { }
}